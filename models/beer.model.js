function Beer() {
    this._beerType;
    this._temp;
    this._id;
}

Beer.toJson = function() {
    var ret = "{"; 
    ret += "    beerType:"+this._beerType.toJson();
    ret += "    temp:"+this._temp;
    ret += "    id:"+this._id;
    ret += "}";
    return ret;
}

Beer.setBeerType = function (beerType) {
    this._beerType = beerType;
}

Beer.getBeerType = function() {
    return this._beerType;
}

Beer.setTemp = function(temp) {
    this._temp = temp;
}

Beer.getTemp = function() {
    return this._temp;
}

Beer.setId = function(id) {
    this._id = id;
}

Beer.getId = function() {
    return this._id;
}


module.exports = function() {
    return Beer;
}
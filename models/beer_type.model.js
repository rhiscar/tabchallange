function BeerType() {
    this._typeName = '';
    this._minTemp = 0;
    this._maxTemp = 0;
}

BeerType.toJson = function() {
    var ret = "{"; 
    ret += "    typeName:"+this._typeName;
    ret += "    minTemp:"+this._minTemp;
    ret += "    maxTemp:"+this._maxTemp;
    ret += "}";
    return ret;
};

BeerType.setTypeName = function(name) {
    this._typeName = name;
}

BeerType.getTypeName = function() {
    return this._typeName
}

BeerType.setMinTemp = function(temp) {
    this._minTemp = temp;
}

BeerType.getMinTemp = function() {
    return this._minTemp;
}

BeerType.setMaxTemp = function (temp) {
    this._maxTemp = temp;
}

BeerType.getMaxTemp = function () {
    return this._maxTemp;
}

module.exports = function() {
    return BeerType;
}